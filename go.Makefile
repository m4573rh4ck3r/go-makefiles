CC						:= go
PROJECT_NAME			:= project
SRCDIR					= $(CURDIR)
BINDIR					= $(CURDIR)/bin
CONFDIR					= $(CURDIR)/etc
LIBDIR					= $(CURDIR)/lib
BINFILE					= $(BINDIR)/$(PROJECT_NAME)
SRCFILE					= $(PROJECT_NAME).go
DOCKERFILE_BUILDER		= Dockerfile
DOCKERFILE_PROD			= prod.Dockerfile
DOCKERFILE_STATIC		= static.Dockerfile
DOCKER_GOOS				?= $$GOOS
DOCKER_GOARCH			?= $$GOARCH
DOCKER_GO111MODULE		:= on
DOCKER_CGO_ENABLED		?= 1
WITH_GPU				:= yes
DOCKER_WORKDIR			:= /go/src/gitlab.com/m4573rh4ck3r/project
DOCKER_SRCDIR			:= $(DOCKER_WORKDIR)
DOCKER_SRCFILE			:= $(DOCKER_SRCDIR)/$(PROJECT_NAME).go
DOCKER_BINDIR			:= $(DOCKER_WORKDIR)/bin
DOCKER_BINFILE			:= $(DOCKER_BINDIR)/$(PROJECT_NAME)
TAG						:= v0
REGISTRY				:= registry.h4ck3r.cc
BUILD_IMAGE_NAME		?= $(REGISTRY)/$(PROJECT_NAME):build
STATIC_IMAGE_NAME		?= $(REGISTRY)/$(PROJECT_NAME):static
PROD_IMAGE_NAME			?= $(REGISTRY)/$(PROJECT_NAME):$(TAG)
GOLINT					:= $(BINDIR)/golint

ifeq ($(DOCKER_GOOS),)
	DOCKER_GOOS := linux
endif

ifeq ($(DOCKER_GOARCH),)
	DOCKER_GOARCH := amd64
endif

ifeq ($(DOCKER_GO111MODULE),)
	DOCKER_GO111MODULE := on
endif

ifeq ($(WITH_GPU),yes)
	DOCKERGOCOMMAND := docker run --gpus all -v $(SRCDIR):$(DOCKER_SRCDIR) -v $(BINDIR):$(DOCKER_BINDIR) --userns host -w $(DOCKER_SRCDIR) -e GOOS=$(DOCKER_GOOS) -e GOARCH=$(DOCKER_GOARCH) -e GO111MODULE=$(DOCKER_GO111MODULE) -e CGO_ENABLED=$(DOCKER_CGO_ENABLED) --rm -it $(BUILD_IMAGE_NAME) $(CC)
else
	DOCKERGOCOMMAND := docker run -v $(SRCDIR):$(DOCKER_SRCDIR) -v $(BINDIR):$(DOCKER_BINDIR) --userns host -w /go/src -e GOOS=$(DOCKER_GOOS) -e GOARCH=$(DOCKER_GOARCH) -e GO111MODULE=$(DOCKER_GO111MODULE) -e CGO_ENABLED=$(DOCKER_CGO_ENABLED) --rm -it $(BUILD_IMAGE_NAME) $(CC)
endif

ifeq ($(WITH_GPU),yes)
	STATICDOCKERCOMMAND := docker run --gpus all -v $(SRCDIR):$(DOCKER_SRCDIR) -v $(BINDIR):$(DOCKER_BINDIR) --userns host -w /go/src -e GOOS=$(DOCKER_GOOS) -e GOARCH=$(DOCKER_GOARCH) -e GO111MODULE=$(DOCKER_GO111MODULE) -e CGO_ENABLED=$(DOCKER_CGO_ENABLED) --rm -it $(STATIC_IMAGE_NAME)
else
	STATICDOCKERCOMMAND := docker run -v $(SRCDIR):$(DOCKER_SRCDIR) -v $(BINDIR):$(DOCKER_BINDIR) --userns host -w /go/src -e GOOS=$(DOCKER_GOOS) -e GOARCH=$(DOCKER_GOARCH) -e GO111MODULE=$(DOCKER_GO111MODULE) -e CGO_ENABLED=$(DOCKER_CGO_ENABLED) --rm -it $(STATIC_IMAGE_NAME)
endif

.PHONY: all
all: build ## run target build

.PHONY: dockerimage
dockerimage: ## build the golang docker image for this project
	@docker build --build-arg UID=$(shell id -u) --build-arg GID=$(shell id -g) --build-arg USERNAME=$(shell whoami) --build-arg GROUPNAME=$(shell whoami) -t $(BUILD_IMAGE_NAME) -f $(DOCKERFILE_BUILDER) .

.PHONY: staticdockerimage
staticdockerimage: ## build the docker image for static compilation
	@docker build --build-arg UID=$(shell id -u) --build-arg GID=$(shell id -g) --build-arg USERNAME=$(shell whoami) --build-arg GROUPNAME=$(shell whoami) -t $(STATIC_IMAGE_NAME) -f $(DOCKERFILE_STATIC) .

$(BINDIR):
	@mkdir -p "$@"

$(BINDIR)/%: | $(BINDIR)

$(BINDIR)/golint:
	GO111MODULE=off GOBIN=$$PWD/bin go get -u golang.org/x/lint/golint

$(CONFDIR):
	@mkdir -p "$@"

$(CONFDIR)/%: | $(CONFDIR)
	@touch $(CONFDIR)/test.txt

$(LIBDIR):
	@mkdir -p "$@"

$(LIBDIR)/%: | $(LIBDIR)
	@touch $(LIBDIR)/%

version:
	@touch version

.PHONY: config
config: | $(CONFDIR) ## create the configuration directory

.PHONY: build
build: dockerimage | $(BINDIR) ## compile the binary
	$(DOCKERGOCOMMAND) build -x -v -o $(DOCKER_BINFILE) $(DOCKER_SRCFILE)

.PHONY: test
test: dockerimage ## run all tests
	$(DOCKERGOCOMMAND) test -race -v ./... | sed -e '/PASS/ s//$(shell printf "\033[32mPASS\033[0m")/' -e '/FAIL/ s//$(shell printf "\033[31mFAIL\033[0m")/' -e '/SKIP/ s//$(shell printf "\033[93mSKIP\033[0m")/'

.PHONY: bench
bench: dockerimage ## run all benchmarks
	@$(DOCKERGOCOMMAND) test -bench ./...

.PHONY: fmt
fmt: ## format all go files
	@$(DOCKERGOCOMMAND) fmt ./...

.PHONY: lint
lint: | $(BINDIR)/golint ## run golint
	@GO111MODULE=off $(GOLINT) -set_exit_status $(shell GO111MODULE=off go list ./... | grep -v bine | grep -v cretz | grep -v tor-static)

.PHONY: get
get: dockerimage ## download all dependencies
	@$(DOCKERGOCOMMAND) get -v "$*"

.PHONY: get/%
get/%: dockerimage ## download a specific dependency
	@$(DOCKERGOCOMMAND) get $*

.PHONY: clean
clean: ## cleanup the build and docker cache
	@docker rmi $(BUILD_IMAGE_NAME)
	@rm -rvf $(BINDIR)
	@go clean -x -v
	@go clean -x -v -cache
	@go clean -x -v -testcache

.PHONY: static
static: | $(BINDIR) ## Build as a static binary
	$(STATICDOCKERCOMMAND) /bin/bash -c "$(CC) build -x -v -o $(DOCKER_BINFILE) -ldflags \"-linkmode external -extldflags -static\" -a $(DOCKER_SRCFILE)"

.PHONY: prod
prod: | version ## create production image
	@docker build --build-arg BINFILE=./bin/$(PROJECT_NAME) -t $(PROD_IMAGE_NAME) -f $(DOCKERFILE_PROD) .

.PHONY: push
push-builder: ## push legacy build image
	@docker push $(PROD_IMAGE_NAME)

.PHONY: push-static
push-static: ## push the image for static compilation
	@docker push $(STATIC_IMAGE_NAME)

.PHONY: push-prod
push-prod: ## push prod image
	@docker push $(PROD_IMAGE_NAME)

.PHONY: tidy
tidy: ## tidy up the dependencies
	$(DOCKERGOCOMMAND) mod tidy

.PHONY: help
help: ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<configurations> <target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
	@awk 'BEGIN {FS = "[ :?=##]"; printf "\nConfigurations:\n"}; length($0)>1 && /^[A-Z]/ {printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
