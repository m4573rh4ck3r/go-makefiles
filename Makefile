# Copyright © 2019 Ruben Mosblech
# This file belongs to the creator
# You may use, modify and distribute this file as long as you keep this notice

MAKEFILENAME	= go.make
MAKEFILE-GO = $(MAKEFILENAME)

.PHONY: all
all: $(MAKEFILE-GO)
	@$(MAKE) -f $(MAKEFILENAME) all || ret=$$?; exit $$ret

%: | $(MAKEFILE-GO)
	@$(MAKE) -f $(MAKEFILENAME) "$@" || ret=$$?; exit $$ret

$(MAKEFILE-GO):
	$(shell curl -Lo $(MAKEFILENAME) https://gitlab.com/m4573rh4ck3r/go-makefiles/-/raw/master/go.Makefile)
